using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public float magnitudSalto;
    public int maxSaltos = 2;
    public int saltoActual = 0;
    private bool EstaEnPiso = true;
    public TMPro.TMP_Text textoMonedasTotales;
    public TMPro.TMP_Text textoGanaste;
    private int cont;
  


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
        cont = 0;
        textoGanaste.text = "";
        setearTextos();

    }

    private void setearTextos()
    {
        textoMonedasTotales.text = "Monedas Totales:  " + cont.ToString();
        if (cont >= 5)
        {
            textoGanaste.text = "Ganaste Lince!";
            
        }
    }


    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);
    }




    private void Update()
    {
        if (Input.GetButtonDown("Jump") && (EstaEnPiso || maxSaltos > saltoActual))
        {
            rb.velocity = new Vector3(0f, magnitudSalto, 0f * Time.deltaTime);
            EstaEnPiso = false;
            saltoActual++;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Parcial");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        EstaEnPiso = true;
        saltoActual = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp") == true)
        {
            rapidez = rapidez + 3;
            this.gameObject.transform.localScale = new Vector3(1, 1, 1);
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

      if (other.gameObject.CompareTag("respawn"))
        {
            SceneManager.LoadScene("Parcial");
        }

      if (other.gameObject.CompareTag("guardian"))
        {
            SceneManager.LoadScene("Parcial");
        }

        if (other.gameObject.CompareTag("debilitador"))
        {
            rapidez = rapidez - 2;
            this.gameObject.transform.localScale = new Vector3(-0.5f, -0.5f, -0.5f);
            other.gameObject.SetActive(false);
        }

    }

   






}
