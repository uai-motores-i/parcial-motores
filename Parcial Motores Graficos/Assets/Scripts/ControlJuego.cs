using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    public TMPro.TMP_Text textoGameOver;
    float tiempoRestante;
    public TMPro.TMP_Text textDisplay;
    public int secondsLeft = 60;
    public bool takingAway = false;
    


    void Start()
    {
        ComenzarJuego();
        textoGameOver.text = "";
        textDisplay.text = "00:" + secondsLeft;
    }

    void Update()
    {
        if (takingAway == false && secondsLeft > 0)
        {
            StartCoroutine(TimerTake());
        }

        
    }



    void ComenzarJuego()
    {
        jugador.transform.position = new Vector3(8f, 4f, 0f);

        
        StartCoroutine(ComenzarCronometro(60));
    }

    IEnumerator TimerTake()
    {
        takingAway = true;
        yield return new WaitForSeconds(1);
        secondsLeft -= 1;
        if (secondsLeft < 10)
        {
            textDisplay.text = "00:0" + secondsLeft;
        }
        else
        {


            textDisplay.text = "00:" + secondsLeft;
        }
        takingAway = false;

    }


    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante >= 0)
        {
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
        
    

        if (tiempoRestante <= 0)
        {
            Time.timeScale = 0;
            textoGameOver.text = "Game Over";

            
        }

       
       





    }
}