using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot1 : MonoBehaviour
{
    private GameObject Jugador;
    public int rapidez;

    void Start()
    {
        Jugador = GameObject.Find("Jugador");

    }

    private void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("jugador"))
        {
            Rigidbody rbJugador = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 lanzarJugador = (collision.gameObject.transform.position - transform.position);
            rbJugador.AddForce(lanzarJugador * 23, ForceMode.Impulse);

        }
    }
}
