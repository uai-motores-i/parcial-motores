using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot4 : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 10;

    void Update()
    {
        if (transform.position.z >= 8)
        {
            tengoQueBajar = true;
        }
        if (transform.position.z <= -12)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }
}
