using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot3 : MonoBehaviour
{

    bool tengoQueBajar = false;
    int rapidez = 6;

    void Update()
    {
        if (transform.position.y >= 10)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 2)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
